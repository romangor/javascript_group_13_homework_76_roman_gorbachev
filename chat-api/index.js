const express = require('express');
const messages = require('./app/messages');
const dataBase = require('./messagesDataBase.js');
const cors = require('cors');

const app = express();
const port = 8000

app.use(express.json());
app.use(cors());
app.use('/messages', messages);

const run = async () => {
    await dataBase.init();
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
}

run().catch(e => console.error(e));
