const express = require('express');
const router = express.Router();
const dataBase = require('../messagesDataBase.js');
const {nanoid} = require('nanoid');

router.get('/', (req, res) => {
    const date = req.query.datetime;
    if (date) {
        const datetime = new Date(date);
        if (isNaN(datetime.getDate())) {
            return res.status(400).send({error: 'Wrong date, please enter a valid date "ex: 2018-04-05T10:02:05.081Z"'});
        } else {
            const messagesByDate = dataBase.getMessagesByDate(datetime.toISOString());
            return res.send(messagesByDate);
        }
    } else {
        const messages = dataBase.getMessages();
        return res.send(messages);
    }

});

router.post('/', async (req, res, next) => {
    try {
        if (!req.body.author || !req.body.message) {
            return res.status(400).send({error: 'Author and message must be present in the request'});
        } else {
            const id = nanoid();
            const date = (new Date()).toISOString()
            const message = {...req.body, id, date};
            await dataBase.addMessages(message);
            return res.send('Your message added');
        }
    } catch (e) {
        next(e);
        return res.send('Error: ' + e.message);
    }
});

module.exports = router;