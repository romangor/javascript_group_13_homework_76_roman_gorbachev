const fs = require('fs').promises;
const base = './data-base.json';

let messages = [];


module.exports = {
    async init() {
        try {
            const messagesFromBase = await fs.readFile(base);
            messages = JSON.parse(messagesFromBase.toString());
        } catch (e) {
            messages = [];
        }
    },

    getMessages() {
        return messages.slice(-31);
    },

    getMessagesByDate(date) {
        let arrayMessages = [];
        messages.forEach(message => {
            if (message.date > date) {
                arrayMessages.push(message);
            } else {
                return arrayMessages;
            }
        });
        return arrayMessages;
    },

    addMessages(message) {
        messages.push(message);
        return this.save();
    },

    save() {
        return fs.writeFile(base, JSON.stringify(messages, null, 1));
    }
}