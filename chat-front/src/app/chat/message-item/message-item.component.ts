import { Component, Input, OnInit } from '@angular/core';
import { Message } from '../../shared/message.model';

@Component({
  selector: 'app-message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.sass']
})
export class MessageItemComponent implements OnInit {
  @Input() message!: Message;

  constructor() { }

  ngOnInit(): void {
  }

}
