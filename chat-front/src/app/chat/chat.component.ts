import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Message } from '../shared/message.model';
import { MessageService } from '../shared/message.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.sass']
})
export class ChatComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  fetchingMessages!: boolean;
  messages!: Message[];

  constructor(private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.messageService.start();
    this.messageService.messagesChange.subscribe((message: Message[]) => {
      this.messages = message;
    });
    this.messageService.fetchingMessages.subscribe((isFetching: boolean) => {
      this.fetchingMessages = isFetching;
    });
  };

  sendMessage() {
    const message = new Message(this.form.value.author, this.form.value.message, '', '');
    this.setFormValue({
      author: '',
      message: '',
    });
    this.messageService.sendMessage(message);
  };

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      this.form.form.setValue(value);
    });
  };

  ngOnDestroy() {
    this.messageService.stop();
  };
}
