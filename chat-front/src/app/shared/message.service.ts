import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from './message.model';
import { interval, map, Subject, Subscription } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messagesChange = new Subject<Message[]>();
  fetchingMessages = new Subject<boolean>();
  subscription!: Subscription;
  messages: Message[] = [];

  constructor(private http: HttpClient) {
  }

  getMessages(date: string) {
    let url = `${environment.url}/messages`
    if (date) {
      url = `${environment.url}/messages?=datetime${date}`
    }
    this.fetchingMessages.next(true);
    this.http.get<{ [id: string]: Message }>(url)
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new Message(data.author, data.message, data.date, data.id)
        })
      }))
      .subscribe(messages => {
        if (!date) {
          this.messages = messages;
          this.fetchingMessages.next(false);
          this.messagesChange.next(this.messages.slice().reverse());
        } else {
          messages.forEach(message => {
            if (message.date > date) {
              this.messages.push(message);
            }
          });
          this.messagesChange.next(this.messages.slice().reverse());
        }
      })
  };

  start() {
    this.subscription = interval(3000).subscribe(() => {
      let lastDate = ''
      if (this.messages.length === 0) {
        this.getMessages(lastDate);
      } else {
        lastDate = this.messages[this.messages.length - 1].date;
        this.getMessages(lastDate);
      }
    });
  };

  stop() {
    this.subscription.unsubscribe();
  };

  sendMessage(message: Message) {
    const body = {
      author: message.author,
      message: message.message
    }
    this.http.post(`${environment.url}/messages`, body).subscribe();
  };
}
