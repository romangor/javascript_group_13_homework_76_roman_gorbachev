export class Message {
  constructor(public author: string,
              public message: string,
              public date: string,
              public id: string
  ) {
  }
}
